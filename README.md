# OS builder
[![pipeline status](https://gitlab.com/matuushos/osbuilder/badges/main/pipeline.svg)](https://gitlab.com/matuushos/osbuilder/-/commits/main)

This is a repository for `osbuilder`, an OS-agnostic builder
## Contributing
Fork the repository and then follow the instructions
```bash
git add <whatever files you want to add>
git commit -am "<type of contribution>: <what you've done>"
git push
```
Then, if you're done, open a PR.

## Example configuration
You'll get one if you run `osbuilder`, but you can also copy this one.
```toml
[buildopts]
tb_name = "mtos_latest"
distro_name = "MatuushOS"
support_url = ""
version = 0.9999
threads = 6
to_fetch = [""]
bdir = ""
dir_structure = "MatuushOS"
xcscriptdir = ""
bscriptdir = ""
```