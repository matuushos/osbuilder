use std::cell::RefCell;
use std::path::Path;

use anyhow::bail;
use flate2::write::GzEncoder;
use flate2::Compression;
use serde::{Deserialize, Serialize};
use url::Url;

#[derive(serde::Deserialize)]
pub struct Cfg {
    pub buildopts: BuildOpts,
}

#[derive(Serialize, Deserialize)]
pub struct BuildOpts {
    pub tb_name: String,
    pub distro_name: String,
    pub support_url: String,
    pub version: f64,
    pub threads: i32,
    pub to_fetch: RefCell<Vec<String>>,
    pub bdir: String,
    pub dir_structure: String,
    pub xcscriptdir: String,
    pub bscriptdir: String,
}
impl From<()> for Cfg {
    fn from(value: ()) -> Self {
        Self {
            buildopts: BuildOpts {
                tb_name: "mtos_latest".to_string().into(),
                distro_name: "matuushos".to_string().into(),
                version: 0.9999.into(),
                threads: 6.into(),
                to_fetch: vec!["".to_string()].into(),
                bdir: "".to_string().into(),
                dir_structure: "matuushos".to_string().into(),
                xcscriptdir: String::from("").into(),
                bscriptdir: String::from("").into(),
                support_url: String::from("").into(),
            },
        }
    }
}
impl Cfg {
    pub fn initialize() -> Self {
        Self {
            buildopts: BuildOpts {
                tb_name: "mtos_latest".to_string().into(),
                distro_name: "matuushos".to_string().into(),
                version: 0.9999.into(),
                threads: 6.into(),
                to_fetch: vec!["".to_string()].into(),
                bdir: "".to_string().into(),
                dir_structure: "matuushos".to_string().into(),
                xcscriptdir: String::from("").into(),
                bscriptdir: String::from("").into(),
                support_url: String::from("").into(),
            },
        }
    }
    pub fn gen_if_notexists(self) -> anyhow::Result<(), anyhow::Error> {
        match Path::new("osbuilder.ron").exists() {
            true => {
                println!(">> Build File exists, everything's OK")
            }
            false => {
                println!(">> Creating the config file right now");
                let cfg = Self::initialize();
                println!(">> Creating config file at {:?}", std::env::current_dir());
                std::fs::write(
                    Path::new("osbuilder.ron"),
                    ron::ser::to_string(&cfg.buildopts)?,
                )?;
                println!(">> DONE\n>> Make sure you edit this file according to your needs or you'll screw it up.");
                std::process::exit(0);
            }
        };
        Ok(())
    }
    pub fn mkdir(&self) -> anyhow::Result<(), anyhow::Error> {
        let ron: Cfg = ron::de::from_str::<Cfg>(&std::fs::read_to_string("osbuilder.ron")?)?;
        match Path::new(Path::new(&ron.buildopts.bdir)).exists() {
            true => std::fs::remove_dir_all(Path::new(&ron.buildopts.bdir))?,
            false => {
                println!(">> Builddir does not exists, creating it right now");
                let mut clfs: String = String::new();
                clfs.push_str(&ron.buildopts.bdir);
                println!(">> Using {} as the build directory", clfs);
                println!(">> Creating build directory");
                std::fs::DirBuilder::new().create(clfs)?;
                std::fs::DirBuilder::new().create(Path::new(&ron.buildopts.bdir).join("vfs"))?;
                std::fs::DirBuilder::new()
                    .create(Path::new(&ron.buildopts.bdir).join("vfs").join("scripts"))?;
                std::fs::DirBuilder::new()
                    .create(Path::new(&ron.buildopts.bdir).join("vfs").join("src"))?;
            }
        }
        println!(">> Build directory created, moving on\n>> Creating the virtual filesystem");
        if &ron.buildopts.dir_structure == "Standard Linux" {
            std::env::set_current_dir(Path::new(&ron.buildopts.bdir).join("vfs"))?;
            for i in ["bin", "lib", "sbin", "usr", "etc", "var"].iter() {
                std::fs::DirBuilder::new().recursive(true).create(i)?;
                println!("\t>> Created directory {:?}", i);
            }
            for sym in ["bin", "lib", "sbin"].iter() {
                #[cfg(target_os = "linux")]
                std::os::unix::fs::symlink(sym, Path::new("usr").join(sym))?;
            }
        }

        println!(">> Copying buildscripts");
        match Path::new(Path::new(&ron.buildopts.bscriptdir)).exists() {
            true => std::fs::copy(
                &ron.buildopts.bscriptdir,
                Path::new(&ron.buildopts.bdir).join("scripts"),
            )?,
            false => {
                bail!(
                    "!! The directory {:?} doesn't exists",
                    Path::new(&ron.buildopts.bscriptdir).to_str()
                );
            }
        };
        println!(">> Downloading sources");
        for dl in ron.buildopts.to_fetch.borrow().iter() {
            let u = Url::parse(dl)?;
            fetch_data::download(
                dl,
                Path::new(&ron.buildopts.bdir)
                    .join("vfs")
                    .join("src")
                    .join(u.path()),
            )?;
            println!("\t>> Downloaded {} from {}", u.path(), dl);
        }
        Ok(())
    }
    pub fn build(self) -> anyhow::Result<(), anyhow::Error> {
        let ron: Cfg = ron::de::from_str::<Cfg>(&std::fs::read_to_string("osbuilder.ron")?)?;
        println!(">> Let's finally build {:?}", ron.buildopts.distro_name);
        println!("\t>> Running crosscompiling scripts");
        for entry in
            walkdir::WalkDir::new(Path::new(&ron.buildopts.bdir).join(&ron.buildopts.xcscriptdir))
                .sort_by_file_name()
        {
            let path = std::path::PathBuf::from(entry?.path());
            if let Some(ext) = path.extension() {
                if ext == "sh" {
                    let path_string = path.display().to_string();
                    vec![path_string.as_str()];
                    std::process::Command::new("sh -c").env(
                        "bdir",
                        Path::new(&ron.buildopts.bdir).join("vfs").join("src"),
                    );
                }
            }
        }
        Ok(())
    }
    pub fn package(self) -> anyhow::Result<(), anyhow::Error> {
        let ron: Cfg = ron::de::from_str::<Cfg>(&std::fs::read_to_string("osbuilder.ron")?)?;
        println!(">> Compressing final file system");
        let mut filename = String::from(&ron.buildopts.tb_name);
        filename.push_str(".tar.gz");
        let f = std::fs::File::create(filename)?;
        let enc = GzEncoder::new(f, Compression::default());
        let mut tar = tar::Builder::new(enc);
        tar.append_dir_all(".", Path::new(&ron.buildopts.bdir).join("vfs"))?;
        Ok(())
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn can_gen_cfg() {
        Cfg::initialize().gen_if_notexists().unwrap();
        assert!(Path::new("osbuilder.ron").exists())
    }
}
